/*************************************************************************
 * Name:         Tony Chen
 * Email:        tonychen@finenet.com.tw
 *
 * Compilation:  javac Solver.java
 * Execution:    java -Xmx3000m Solver puzzle04.txt
 *                  {manhattan:1} {hamming:0} {stopOnGoal:1} {debug:0}
 * Dependencies: Board.java
 *
 * Description: Implement the A* algorithm to solve a N-puzzle problem.
 * It should work correctly for arbitrary N-by-N boards (for any 2 ≤ N < 128)
 *************************************************************************/

import java.util.HashMap;

public class Solver {
    private static final int MAX_HASH1 =  1000000;  //  1 M
    private static final int MAX_HASH2 = 10000000;  // 10 M
    private static int multiManhattan = 1;          // args[0]: 0 ~
    private static int multiHamming = 0;            // args[1]: 0 ~
    private static boolean stopOnGoal = true;       // args[2]: 0 | 1
    private static int debug = 0;                   // args[3]: 0 | 1 | 2

    private Node solvedNode;
    private boolean solved;

    private class Node implements Comparable<Node> {
        private Board board;
        private Node prev;
        private int moves;
        private int priority;

        public Node(Board board, Node parent, int moves, int multi) {
            this.board = board;
            this.prev = parent;
            this.moves = moves;

            int prio = moves;
            if (multiManhattan > 0) {
                prio += multi * multiManhattan * board.manhattan();
            }
            if (multiHamming > 0) {
                prio += multi * multiHamming   * board.hamming();
            }
            this.priority = prio;
        }

        // is this node lexicographically less than that one?
        public int compareTo(Node that) {
            int sts = this.priority - that.priority;
            if (sts != 0) return sts;

            boolean a = this.isGoal();
            boolean b = that.isGoal();
            if (a == b) {
                return Solver.stopOnGoal ? that.moves - this.moves : 0;
            }
            if (a) return -1;
            return 1;
        }

        public boolean isGoal() {
            return board.isGoal();
        }

        public String toString() {
            return board.toString1();   /* !!debug */
        }

        public Board lastBoard() {
            if (prev != null) return prev.board;
            return null;
        }

        // Estimate minimum number of moves
        public int estimateMinMoves() {
            if (moves < 0) return Integer.MAX_VALUE;
            return moves + board.manhattan() + 1;
        }
    }

    private class Puzzle {
        private String title;
        private Node current;
        private Node goal;
        private MinPQ<Node> pq;
        private HashMap<Board, Node> hash;
        private int movesLimited = Integer.MAX_VALUE;
        private int multi;
        private int debug;
        private boolean stopOnGoal;

        public Puzzle(String name, Board initial, int multi,
                    boolean stopOnGoal, int debug) {
            title = name;
            pq = new MinPQ<Node>();
            hash = new HashMap<Board, Node>();
            this.multi = multi;
            this.debug = debug;
            this.stopOnGoal = stopOnGoal;
            addToQueue(initial);
        }

        public boolean isSolvable() {
            return goal != null;
        }

        public boolean isDone() {
            return pq.isEmpty();
        }

        public void status(final String msg) {
            /* !!debug */ if (Solver.debug == 0) return;
            if (hash != null || goal != null) {
                int hsize = 0;
                if (hash != null) hsize = hash.size();
                String s = "";
                if (goal != null) s = String.format(", %d moves", goal.moves);
                s += msg;
                StdOut.println(String.format(
                    "%s: hash = %d, pq = %d%s",
                    title, hsize, pq.size(), s));
            }
        }

        private boolean addToQueue(Board board) {
            int moves = 0;
            if (current != null) {
                // [last.board] -> [current.board] -> [board]
                // quick check board == last.board
                if (board.equals(current.lastBoard())) return false;
                moves = current.moves + 1;
            }

            boolean isGoal = board.isGoal();
            if (!isGoal) {
                Node stock = hash.get(board);
                if (stock != null) {
                    if (moves >= stock.moves) return false;

                    /* !!debug 1 */ if (debug > 1)
                    StdOut.println(String.format(
                        "= [%s] %d:%d <- %d moves",
                        stock.toString(), stock.moves, stock.priority,
                        moves));

                    // Update stock.moves to -1
                    // so when the stock pop from pq, it will be discard
                    stock.moves = -1;
                }
            }

            Node stock = new Node(board, current, moves, multi);

            if (isGoal) {
                if (moves < movesLimited) movesLimited = moves;

            /*****************************************************************
             * Checklist: 8 Puzzle - Frequently Asked Questions
             *
             * Can I terminate the search as soon as a goal search node is
             * enqueued (instead of dequeued)? No, even though it does lead
             * to a correct solution for the slider puzzle problem using the
             * Hamming and Manhattan priority functions, it's not techincally
             * the A* algorithm (and will not find the correct solution for
             * other problems and other priority functions).
             *****************************************************************/
                // if (stopOnGoal) {
                //     goal = stock;
                //     return false;
                // }

                /* !!debug 1 */ if (debug > 0)
                status(" <-- push goal: "+moves+" moves");
            }

            // add stock (new board) to pq & hash
            pq.insert(stock);
            hash.put(board, stock);

            /* !!debug 1 */ if (debug > 1)
            StdOut.println(String.format(
                "+ [%s] %d:%d",
                stock.toString(), moves, stock.priority));

            return true;
        }

        private void run() {
            while (!isDone()) {
                current = pq.delMin();
                int moves = current.moves;
                if (moves < 0) {
                    /* !!debug 1 */ if (debug > 1)
                    StdOut.println(String.format(
                        "- [%s] --:%d",
                        current.toString(), current.priority));
                    continue;
                } else {
                    /* !!debug 1 */ if (debug > 1)
                    StdOut.println(String.format(
                        "- [%s] %d:%d",
                        current.toString(), moves, current.priority));
                }

                if (current.isGoal()) {
                    if (goal == null || moves < goal.moves) {
                        goal = current;
                        if (stopOnGoal) break;

                        if (pq.size() >= MAX_HASH1) {
                            // remove nodes from pq which goes too far
                            MinPQ<Node> pq1 = new MinPQ<Node>();
                            while (!pq.isEmpty()) {
                                Node t = pq.delMin();
                                if (t.estimateMinMoves() < moves) {
                                    pq1.insert(t);
                                }
                            }
                            pq = pq1;
                            status(" <-- new goal & pq[]");
                        } else {
                            status(" <-- new goal");
                        }
                    }
                    break;
                }

                if (current.estimateMinMoves() < movesLimited) {
                    for (Board board : current.board.neighbors()) {
                        addToQueue(board);
                    }
                    break;
                }
            }
        }
    }

    // find a solution to the initial board (using the A* algorithm)
    public Solver(Board initial) {
        Puzzle goal = new Puzzle("goal", initial, 1, stopOnGoal, debug);
        Puzzle twin = new Puzzle("twin", initial.twin(), 1024, true, 0);

        while (!goal.isDone() && !twin.isSolvable()) {
            goal.run();
            if (goal.isSolvable()) {
                if (stopOnGoal || goal.hash.size() >= MAX_HASH1) break;
                if (!twin.isDone()) {
                    twin.status(" <-- disable: unsolvable");
                    twin.pq = new MinPQ<Node>();
                    twin.hash = null;
                }
            } else if (!twin.isDone()) {
                twin.run();
                if (twin.hash.size() >= MAX_HASH1) {
                    // disable twin when it runs too long
                    twin.status(" <-- disable: runs too long");
                    twin.pq = new MinPQ<Node>();
                    twin.hash = null;
                }

            }
            if (goal.hash.size() >= MAX_HASH2) break;
        }

        twin.status(" <-- ending");

        String s;
        if (goal.isSolvable()) {
            s = "solver";
            solved = true;
            solvedNode = goal.goal;
        } else {
            if (twin.isSolvable())  s = "unsolvable";
            else                    s = "give up";
        }
        goal.status(" <-- "+s);
    }

    // is the initial board solvable?
    public boolean isSolvable() {
        return solved;
    }

    // min number of moves to solve initial board; -1 if no solution
    public int moves() {
        if (solvedNode == null) return -1;
        return solvedNode.moves;
    }

    // sequence of boards in a shortest solution; null if no solution
    public Iterable<Board> solution() {
        if (!solved) return null;

        Stack<Board> result = new Stack<Board>();
        Node next = solvedNode;
        while (next != null) {
            result.push(next.board);
            next = next.prev;
        }
        return result;
    }

    // solve a slider puzzle (given below)
    public static void main(String[] args) {
        boolean stat = false;
        if (args.length > 1) {
            stat = true;
            multiManhattan = Integer.parseInt(args[1]);
            if (args.length > 2) multiHamming = Integer.parseInt(args[2]);
            if (multiManhattan == 0 && multiHamming == 0) multiManhattan = 1;
            if (args.length > 3) stopOnGoal = !args[3].equals("0");
            if (args.length > 4) debug = Integer.parseInt(args[4]);
        }

        // create initial board from file
        In in = new In(args[0]);
        int N = in.readInt();
        int[][] blocks = new int[N][N];
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                blocks[i][j] = in.readInt();
        Board initial = new Board(blocks);

        Stopwatch t1 = new Stopwatch();
        StopwatchCPU t2 = new StopwatchCPU();

        // solve the puzzle
        Solver solver = new Solver(initial);

        if (stat) {
            StdOut.println(String.format(
                "%s: %d moves, M=%d, H=%d, T1=%.4g, T2=%.4g",
                args[0], solver.moves(), multiManhattan, multiHamming,
                t1.elapsedTime(), t2.elapsedTime()));
        } else {
            // print solution to standard output
            if (!solver.isSolvable())
                StdOut.println("No solution possible");
            else {
                StdOut.println("Minimum number of moves = " + solver.moves());
                for (Board board : solver.solution())
                    StdOut.println(board);
            }
        }
    }

}

