# Makefile
# vim: set noet ts=8 sw=8:

SOURCE=Board.java Solver.java
TARGET=$(SOURCE:.java=.class)

.PHONY: all compile clean work test help checkstyle findbugs zip

%.class: %.java
	@/bin/echo -n "% "
	javac-algs4 $<

compile: $(TARGET)
	@echo "Compiling done."

all: test work
	@echo "All done."

work: checkstyle findbugs zip

clean:
	rm -f *.class *.zip

checkstyle: $(SOURCE)
	@/bin/echo -n "% "
	checkstyle-algs4 $^

findbugs: $(SOURCE:.java=.class)
	@/bin/echo -n "% "
	findbugs-algs4 *.class

help:
	@echo "usage: make [all | checkstyle | findbugs | zip | test]"

zip: 8puzzle.zip

8puzzle.zip: $(SOURCE)
	@/bin/echo -n "% "
	zip -u $@ $^

test: $(TARGET)
	@/bin/echo -n "% "
	java-algs4 Solver data/puzzle04.txt
	@/bin/echo -n "% "
	java-algs4 Solver data/puzzle3x3-unsolvable.txt

check: PuzzleChecker.class $(TARGET)
