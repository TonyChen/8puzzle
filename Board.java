/*************************************************************************
 * Name:         Tony Chen
 * Email:        tonychen@finenet.com.tw
 *
 * Compilation:  javac Board.java
 * Execution:    java Board
 *
 * Description:
 *
 *************************************************************************/

import java.util.Iterator;

public class Board {

    private short[] tiles;              // N-by-N grid, decrease every cells
    private short N;                    // 0 ~ 127
    private short size;                 // N x N
    private short emptyPos;             // 0 ~ N-1
    private short stsGoal = -1;         // -1: not test, 0: not goal, 1: goal

    private int calcHamming = -1;
    private int calcManhattan = -1;
    private int hcode = Integer.MIN_VALUE;

    // construct a board from an N-by-N array of blocks
    // (where blocks[i][j] = block in row i, column j)
    public Board(int[][] blocks) {
        N = (short) blocks.length;
        if (N < 2 || N >= 128) {
            throw new java.lang.IllegalArgumentException(
                "board dimension should be 2~127");
        }

        size = (short) (N * N);
        tiles = new short[size];
        int k = 0;
        for (int i = 0; i < blocks.length; i++) {
            for (int j = 0; j < blocks[i].length; j++) {
                int val = blocks[i][j];
                if (val < 0 || val > size) {
                    throw new java.lang.RuntimeException(String.format(
                        "blocks[%d,%d]=%d, the value must be 0~%d",
                        i, j, val, size));
                }

                if (val == 0) {     // change empty value to (size)
                    emptyPos = (short) k;
                    val = (short) size;
                }
                tiles[k++] = (short) (val-1);
            }
        }

        if (k != size) {
            throw new java.lang.IllegalArgumentException(
                "blocks is not a "+N+"-by-"+N+" grid");
        }
    }

    private Board(Board board) {
        this.N = board.N;
        this.size = board.size;
        this.emptyPos = board.emptyPos;
        this.tiles = board.tiles.clone();
    }

    // board dimension N
    public int dimension() {
        return N;
    }

    // number of blocks out of place
    public int hamming() {
        if (calcHamming < 0) {
            int result = 0;
            for (int i = 0; i < size-1; i++) {
                if (tiles[i] != i) result++;
            }
            calcHamming = result;
        }
        return calcHamming;
    }

    // sum of Manhattan distances between blocks and goal
    public int manhattan() {
        if (calcManhattan < 0) {
            int result = 0;
            int row = 0;
            int col = 0;
            for (int i = 0; i < size; i++) {
                int val = tiles[i];
                if (val < size-1) {                 // emptyMark = size-1
                    if (val != i) {
                        int diff = val / N - row;
                        if (diff < 0) result -= diff;
                        else          result += diff;

                        diff = val % N - col;
                        if (diff < 0) result -= diff;
                        else          result += diff;
                    }
                }
                if (++col >= N) {
                    row++;
                    col = 0;
                }
            }
            calcManhattan = result;
        }
        return calcManhattan;
    }

    // is this board the goal board?
    public boolean isGoal() {
        // emptyMark = size-1
        if (emptyPos != size-1) return false;

        if (stsGoal < 0) {
            stsGoal = 1;
            for (int i = 0; i < size; i++) {
                if (tiles[i] != i) {
                    stsGoal = 0;
                    break;
                }
            }
        }
        return stsGoal == 1;
    }

     @Override
    public int hashCode() {
        if (hcode == Integer.MIN_VALUE) {
            hcode = java.util.Arrays.hashCode(tiles);
        }
        return hcode;
    }

    // a board obtained by exchanging two adjacent blocks in the same row
    public Board twin() {
        Board result = new Board(this);

        int p1, p2;
        if (emptyPos >= N) {
            p1 = 0;
            p2 = 1;
        } else {
            p1 = N;
            p2 = N+1;
        }
        result.tiles[p1] = tiles[p2];
        result.tiles[p2] = tiles[p1];

        return result;
    }

    // does this board equal y?
    public boolean equals(Object y) {
        if (y == this) return true;
        if (y == null) return false;
        if (y.getClass() != this.getClass()) return false;

        Board that = (Board) y;
        if (this.N != that.N || this.emptyPos != that.emptyPos) return false;

        for (int i = 0; i < size; i++) {
            if (this.tiles[i] != that.tiles[i]) return false;
        }

        return true;
    }

    // all neighboring boards
    public Iterable<Board> neighbors() {
        return new Neighbors();
    }

    private class Neighbors implements Iterable<Board> {
        public Iterator<Board> iterator() { return new ListNeighbors(); }

        private class ListNeighbors implements Iterator<Board> {
            private int current = 0;        // 1:up, 2:right, 3:down, 4:left
            private int targetPos;

            public ListNeighbors() {
                findNext();
            }

            public boolean hasNext() {
                return current <= 4;
            }

            public void remove() {
                throw new UnsupportedOperationException();
            }

            public Board next() {
                if (current > 4) {
                    throw new java.util.NoSuchElementException();
                }

                Board result = new Board(Board.this);
                // move from targetPos to emptyPos
                result.tiles[result.emptyPos] = result.tiles[targetPos];
                result.tiles[targetPos] = (short) (size-1);     // emptyMark
                result.emptyPos = (short) targetPos;

                findNext();
                return result;
            }

            private void findNext() {
                while (current < 4) {
                    current++;
                    targetPos = -1;
                    switch(current) {
                        case 1: // up
                            targetPos = emptyPos-N;
                            break;
                        case 2: // right
                            if ((emptyPos+1) % N != 0) targetPos = emptyPos+1;
                            break;
                        case 3: // down
                            targetPos = emptyPos+N;
                            break;
                        case 4: // left
                            if (emptyPos % N != 0) targetPos = emptyPos-1;
                            break;
                        default:
                            if (current <= 0) {
                                throw new java.lang.RuntimeException(
                                    "Invalid state for current = "+current);
                            }
                            break;
                    }
                    if (targetPos >= 0 && targetPos < size) return;
                }
                current = 5;
            }
        }
    }

    // string representation of the board (in the output format specified below)
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append(N + "\n");
        for (int i = 0; i < size; i++) {
            int val = tiles[i] + 1;
            if (val >= size) {                  // emptyMark = size-1
                s.append(" 0 ");
            } else {
                s.append(String.format("%2d ", val));
            }

            if ((i+1) % N == 0) s.append("\n");
        }
        return s.toString();
    }

    public String toString1() {
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < size; i++) {
            int val = tiles[i] + 1;
            if (val >= size) {                  // emptyMark = size-1
                s.append(" _ ");
            } else {
                s.append(String.format("%2d ", val));
            }
        }
        return s.toString();
    }

    public static void main(String[] args) {
        // create initial board from file
        In in = new In(args[0]);
        int N = in.readInt();
        int[][] blocks = new int[N][N];
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                blocks[i][j] = in.readInt();
            }
        }

        Board initial = new Board(blocks);
        StdOut.print("Board = ");
        StdOut.println(initial);

        StdOut.print("Twin = ");
        StdOut.println(initial.twin());

        for (Board board : initial.neighbors()) {
            StdOut.print("Neighbors = ");
            StdOut.println(board);
        }
    }

}
